/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solemne3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Juan Ramirez
 */
@Entity
@Table(name = "clientes")
@NamedQueries({
    @NamedQuery(name = "Clientes.findAll", query = "SELECT c FROM Clientes c"),
    @NamedQuery(name = "Clientes.findByNombreEmpresa", query = "SELECT c FROM Clientes c WHERE c.nombreEmpresa = :nombreEmpresa"),
    @NamedQuery(name = "Clientes.findByMonto", query = "SELECT c FROM Clientes c WHERE c.monto = :monto"),
    @NamedQuery(name = "Clientes.findByA", query = "SELECT c FROM Clientes c WHERE c.a = :a"),
    @NamedQuery(name = "Clientes.findByA1", query = "SELECT c FROM Clientes c WHERE c.a1 = :a1")})
public class Clientes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre empresa")
    private String nombreEmpresa;
    @Size(max = 2147483647)
    @Column(name = "monto")
    private String monto;
    @Size(max = 2147483647)
    @Column(name = "")
    private String a;
    @Size(max = 2147483647)
    @Column(name = "")
    private String a1;

    public Clientes() {
    }

    public Clientes(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombreEmpresa != null ? nombreEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clientes)) {
            return false;
        }
        Clientes other = (Clientes) object;
        if ((this.nombreEmpresa == null && other.nombreEmpresa != null) || (this.nombreEmpresa != null && !this.nombreEmpresa.equals(other.nombreEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.solemne3.entity.Clientes[ nombreEmpresa=" + nombreEmpresa + " ]";
    }
    
}
